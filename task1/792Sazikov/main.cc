#include <utils/application.h>
#include <utils/shader_program.h>

#include <iostream>

#include "mesh_loader.h"

class TMyApplication final : public TApplication {
 private:
  void MakeScene() override {
    TApplication::MakeScene();
    TerrainMesh_ = LoadTerrainMesh(0.1f, "792SazikovData1/lroc_color_poles_2k.png");
    TerrainMesh_->SetModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(-60.0f, -60.0f, -1.0f)));
    CameraMover_ = std::make_unique<TFreeCameraMover>();
    ShaderProgram_ =
        std::make_unique<TShaderProgram>("792SazikovData1/vert_shader.glsl", "792SazikovData1/frag_shader.glsl");
  }

  void Draw() override {
    int width, height;
    glfwGetFramebufferSize(Window_, &width, &height);
    glViewport(0, 0, width, height);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    ShaderProgram_->Use();

    ShaderProgram_->SetUniform("viewMatrix", Camera_.ViewMatrix);
    ShaderProgram_->SetUniform("projectionMatrix", Camera_.ProjMatrix);
    ShaderProgram_->SetUniform("modelMatrix", TerrainMesh_->ModelMatrix());

    TerrainMesh_->Draw();
  }

  void UpdateGUI() override {
    TApplication::UpdateGUI();
    ImGui::SetNextWindowPos(ImVec2(0, 0), ImGuiSetCond_FirstUseEver);
    if (ImGui::Begin("MIPT OpenGL", nullptr, ImGuiWindowFlags_AlwaysAutoResize)) {
      ImGui::Text("FPS %.0f", ImGui::GetIO().Framerate);
    }
    ImGui::End();
  }

 private:
  TMeshHolder TerrainMesh_;
  TShaderProgramHolder ShaderProgram_;
};

int main() {
  try {
    TMyApplication app;
    app.Start();
  } catch(const std::exception& e) {
    std::cout << "Unhandled exception\n" << e.what() << std::endl;
    return 1;
  }
  return 0;
}