#pragma once

#include <utils/mesh.h>

#include <string_view>

TMeshHolder LoadTerrainMesh(float scale, std::string_view file);