#pragma once

#include <string_view>
#include <vector>

class THeightsMap {
 public:
  explicit THeightsMap(std::string_view path) { LoadFromFile(path); }

  void LoadFromFile(std::string_view path);

  [[nodiscard]] float GetHeight(size_t i, size_t j) const {
    i = std::min(i, GetXSize() - 1);
    j = std::min(j, GetYSize() - 1);
    return Heights_[i][j];
  }

  [[nodiscard]] size_t GetYSize() const { return Heights_.empty() ? 0 : Heights_[0].size(); }

  [[nodiscard]] size_t GetXSize() const { return Heights_.size(); }

 private:
  void LoadFromData(const unsigned char* data, int width, int height);

 private:
  std::vector<std::vector<float>> Heights_;
};