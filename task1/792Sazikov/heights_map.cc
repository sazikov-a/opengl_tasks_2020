#include "heights_map.h"

#include <SOIL2.h>

#include <memory>
#include <stdexcept>
#include <string>

void THeightsMap::LoadFromFile(std::string_view path) {
  int width, height, channels;
  std::unique_ptr<unsigned char, decltype(&SOIL_free_image_data)> data(
      SOIL_load_image(path.data(), &width, &height, &channels, SOIL_LOAD_L), &SOIL_free_image_data);

  if (channels != 1) {
    throw std::runtime_error("expected 1 channel for " + std::string(path) + ", found: " + std::to_string(channels));
  }

  LoadFromData(data.get(), width, height);
}

void THeightsMap::LoadFromData(const unsigned char* data, int width, int height) {
  Heights_.resize(height);
  for (int i = 0; i < height; ++i, data += width) {
    Heights_[i].resize(width);
    for (int j = 0; j < width; ++j) {
      Heights_[i][j] = static_cast<float>(data[j]) / 255;
    }
  }
}
