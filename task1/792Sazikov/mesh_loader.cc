#include "mesh_loader.h"

#include "heights_map.h"

auto TriangleNorm(glm::vec3 v1, glm::vec3 v2, glm::vec3 v3) { return glm::cross(v1 - v2, v1 - v3); }

TMeshHolder LoadTerrainMesh(float scale, std::string_view file) {
  std::vector<glm::vec3> vertices;
  std::vector<glm::vec3> normals;
  auto heightsMap = THeightsMap(file);

  for (size_t i = 0; i < heightsMap.GetXSize() - 1; i++) {
    for (size_t j = 0; j < heightsMap.GetYSize() - 1; j++) {
      auto v1 = glm::vec3(scale * (float)i, (float)j * scale, heightsMap.GetHeight(i, j));
      auto v2 = glm::vec3((float)(i + 1) * scale, (float)j * scale, heightsMap.GetHeight(i + 1, j));
      auto v3 = glm::vec3((float)i * scale, (float)(j + 1) * scale, heightsMap.GetHeight(i, j + 1));
      auto v4 = glm::vec3((float)(i + 1) * scale, (float)(j + 1) * scale, heightsMap.GetHeight(i + 1, j + 1));

      auto n1 = TriangleNorm(v1, v2, v3);
      auto n2 = TriangleNorm(v2, v3, v4);

      vertices.emplace_back(v1);
      vertices.emplace_back(v2);
      vertices.emplace_back(v3);
      vertices.emplace_back(v2);
      vertices.emplace_back(v3);
      vertices.emplace_back(v4);

      normals.emplace_back(n1);
      normals.emplace_back(n1);
      normals.emplace_back(n1);
      normals.emplace_back(n2);
      normals.emplace_back(n2);
      normals.emplace_back(n2);
    }
  }

  auto verticesBuffer = std::make_unique<TDataBuffer>(GL_ARRAY_BUFFER);
  verticesBuffer->SetData(vertices.size() * sizeof(float) * 3, vertices.data());

  auto normalsBuffer = std::make_unique<TDataBuffer>(GL_ARRAY_BUFFER);
  normalsBuffer->SetData(normals.size() * sizeof(float) * 3, normals.data());

  auto mesh = std::make_unique<TMesh>();
  mesh->SetAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, verticesBuffer.release());
  mesh->SetAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, normalsBuffer.release());
  mesh->SetPrimitiveType(GL_TRIANGLES);
  mesh->SetVertexCount(vertices.size());

  return std::move(mesh);
}
