#include "surface_camera.h"

#include <glm/gtx/quaternion.hpp>
#include <glm/gtx/transform.hpp>
#include <utility>

TSurfaceCameraMover::TSurfaceCameraMover(std::function<float(float, float)> heights)
    : TCameraMover()
    , Position_(5.f, 0.f, 3.f)
    , Rotation_(glm::lookAt(Position_, glm::vec3(0.0f, 0.0f, 0.5f), glm::vec3(0.0f, 0.0f, 1.0f)))
    , HeightAt_(std::move(heights)) {}

void TSurfaceCameraMover::HandleKey(GLFWwindow* window, int key, int scancode, int action, int mods) {}

void TSurfaceCameraMover::HandleMouseMove(GLFWwindow* window, double xPos, double yPos) {
  if (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT) == GLFW_PRESS) {
    double dx = xPos - OldXPos_;
    double dy = yPos - OldYPos_;

    Rotation_ *= glm::angleAxis(static_cast<float>(dy * 0.005), glm::vec3(1.0f, 0.0f, 0.0f) * Rotation_);
    Rotation_ *= glm::angleAxis(static_cast<float>(dx * 0.005), glm::vec3(0.0f, 0.0f, 1.0f));
  }

  OldXPos_ = xPos;
  OldYPos_ = yPos;
}

void TSurfaceCameraMover::HandleScroll(GLFWwindow* window, double xOffset, double yOffset) {}

void TSurfaceCameraMover::Update(GLFWwindow* window, double dt) {
  static constexpr float speed = 1.0f;

  glm::vec3 forwardDir = glm::vec3(0.0f, 0.0f, -1.0f) * Rotation_;
  glm::vec3 rightDir = glm::vec3(1.0f, 0.0f, 0.0f) * Rotation_;

  if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS) {
    Position_ += forwardDir * speed * static_cast<float>(dt);
  }
  if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS) {
    Position_ -= forwardDir * speed * static_cast<float>(dt);
  }
  if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS) {
    Position_ -= rightDir * speed * static_cast<float>(dt);
  }
  if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS) {
    Position_ += rightDir * speed * static_cast<float>(dt);
  }

  Position_.z = HeightAt_(Position_.x, Position_.y);

  int width, height;
  glfwGetFramebufferSize(window, &width, &height);

  Camera_.ViewMatrix = glm::toMat4(-Rotation_) * glm::translate(-Position_);
  Camera_.ProjMatrix = glm::perspective(glm::radians(45.0f), (float)width / (float)height, Near_, Far_);
}
