#include <heights_map.h>
#include <mesh_loader.h>
#include <utils/application.h>
#include <utils/shader_program.h>
#include <utils/texture_mixer.h>

#include <array>
#include <iostream>

#include "surface_camera.h"

class TMyApplication final : public TApplication {
 private:
  void MakeScene() override {
    static constexpr float scale = 0.1f;
    static constexpr auto terrainFile = "792SazikovData2/lroc_color_poles_2k.png";

    TApplication::MakeScene();

    TerrainMesh_ = LoadTerrainMesh(scale, terrainFile);

    TerrainMesh_->SetModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(-60.0f, -60.0f, -.5f)));

    CameraMover_ = std::make_unique<TSurfaceCameraMover>([&, heightsMap = THeightsMap(terrainFile)](float x, float y) {
      return heightsMap.GetHeight(static_cast<size_t>(x / scale), static_cast<size_t>(y / scale)) + 0.2f;
    });

    ShaderProgram_ =
        std::make_unique<TShaderProgram>("792SazikovData2/vert_shader.glsl", "792SazikovData2/frag_shader.glsl");

    Textures_ = CreateTextureMixer<5>(TexturePaths_);
  }

  void Draw() override {
    int width, height;
    glfwGetFramebufferSize(Window_, &width, &height);
    glViewport(0, 0, width, height);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    ShaderProgram_->Use();

    ShaderProgram_->SetUniform("viewMatrix", Camera_.ViewMatrix);
    ShaderProgram_->SetUniform("projectionMatrix", Camera_.ProjMatrix);
    ShaderProgram_->SetUniform("modelMatrix", TerrainMesh_->ModelMatrix());
    ShaderProgram_->SetUniform("lightDirection", glm::vec4(1.0, 0.0, -1.0, 1.0));

    Textures_->Draw(ShaderProgram_.get(), TextureFiles_);

    TerrainMesh_->Draw();
  }

  void UpdateGUI() override {
    TApplication::UpdateGUI();
    ImGui::SetNextWindowPos(ImVec2(0, 0), ImGuiSetCond_FirstUseEver);
    if (ImGui::Begin("MIPT OpenGL", nullptr, ImGuiWindowFlags_AlwaysAutoResize)) {
      ImGui::Text("FPS %.0f", ImGui::GetIO().Framerate);
    }
    ImGui::End();
  }

 private:
  TMeshHolder TerrainMesh_;
  TShaderProgramHolder ShaderProgram_;
  TTextureMixerPtr<5> Textures_;

  std::array<std::string_view, 5> TexturePaths_ = {"792SazikovData2/snow.jpg", "792SazikovData2/dirt.jpg",
                                                   "792SazikovData2/grass.jpg", "792SazikovData2/rock.jpg",
                                                   "792SazikovData2/base.jpg"};
  std::array<std::string_view, 5> TextureFiles_ = {"snow", "dirt", "grass", "rock", "base"};
};

int main() {
  try {
    TMyApplication app;
    app.Start();
  } catch (const std::exception& e) {
    std::cout << "Unhandled exception\n" << e.what() << std::endl;
    return 1;
  }
  return 0;
}