#version 330

uniform sampler2D snow;
uniform sampler2D dirt;
uniform sampler2D grass;
uniform sampler2D rock;
uniform sampler2D base;

in vec3 normalInCameraView;
in vec4 positionInCameraView;
in vec2 textureCoordinat;
in vec4 lightDirectionInCameraView;

out vec4 fragColor;

const vec3 La = vec3(1.0, 1.0, 1.0) * 0.1;
const vec3 Ld = vec3(1.0, 1.0, 1.0) * 0.5;
const vec3 Ls = vec3(1.0, 1.0, 1.0) * 0.1;

const vec4 Km = normalize(vec4(1.0, 0.1, 0.5, 0.3));
const vec3 Ks = vec3(1.0, 1.0, 1.0);
const float shininess = 128.0;


void main() {
    vec4 textureColorCoeff = normalize(texture(base, textureCoordinat / 2.0));
    vec3 textureColor =
    texture(snow, textureCoordinat).rgb * textureColorCoeff[0] +
    texture(dirt, textureCoordinat).rgb * textureColorCoeff[1] +
    texture(grass, textureCoordinat).rgb * textureColorCoeff[2] +
    texture(rock, textureCoordinat).rgb * textureColorCoeff[3];


    vec3 normal = normalize(normalInCameraView);
    vec3 position = normalize(positionInCameraView.xyz);

    float dotProduct = max(dot(normalInCameraView, lightDirectionInCameraView.xyz), 0.0);
    vec3 color = textureColor * La + textureColor * Ld * dotProduct;

    if (dotProduct > 0.0) {
        vec3 h = normalize(position + lightDirectionInCameraView.xyz);

        float bt = pow(max(dot(position, h), 0.0), shininess * length(textureColor));

        float cs = dot(Km, textureColorCoeff);

        color += Ls * Ks * cs * bt;
    }

    fragColor = vec4(color, 1.0);
}
