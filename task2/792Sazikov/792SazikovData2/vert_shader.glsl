#version 330

uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;
uniform vec4 lightDirection;

layout(location = 0) in vec3 vertex;
layout(location = 1) in vec3 normal;

out vec3 normalInCameraView;
out vec4 positionInCameraView;
out vec2 textureCoordinat;
out vec4 lightDirectionInCameraView;

void main() {
    textureCoordinat = vertex.xy;
    lightDirectionInCameraView = viewMatrix * normalize(lightDirection);
    positionInCameraView = viewMatrix * vec4(vertex, 1.0);
    normalInCameraView = normalize(transpose(inverse(mat3(viewMatrix))) * normal);

    gl_Position = projectionMatrix * viewMatrix * modelMatrix * vec4(vertex, 1.0);
}
