#pragma once

#include <heights_map.h>
#include <utils/camera.h>

#include <functional>

class TSurfaceCameraMover final : public TCameraMover {
 public:
  explicit TSurfaceCameraMover(std::function<float(float, float)> heights);

  void HandleKey(GLFWwindow* window, int key, int scancode, int action, int mods) override;
  void HandleMouseMove(GLFWwindow* window, double xPos, double yPos) override;
  void HandleScroll(GLFWwindow* window, double xOffset, double yOffset) override;

  void Update(GLFWwindow* window, double dt) override;

 private:
  glm::vec3 Position_;
  glm::quat Rotation_;

  double OldXPos_ = 0;
  double OldYPos_ = 0;

  std::function<float(float, float)> HeightAt_;
};