#include "debug_output.h"

#include <cassert>
#include <iostream>
#include <string>
#include <unordered_map>

#define RECORD(enumPrefix, glenum) \
  { GL_DEBUG_##enumPrefix##_##glenum, #glenum }
#define RECORD3(enumPrefix, glenum, val) \
  { GL_DEBUG_##enumPrefix##_##glenum, val }

static std::unordered_map<GLenum, std::string> debugSourceMapping = {
    RECORD(SOURCE, API),         RECORD(SOURCE, WINDOW_SYSTEM), RECORD(SOURCE, SHADER_COMPILER),
    RECORD(SOURCE, THIRD_PARTY), RECORD(SOURCE, APPLICATION),   RECORD(SOURCE, OTHER)};

static std::unordered_map<GLenum, std::string> debugTypeMapping = {
    RECORD(TYPE, ERROR),       RECORD(TYPE, DEPRECATED_BEHAVIOR), RECORD(TYPE, UNDEFINED_BEHAVIOR),
    RECORD(TYPE, PORTABILITY), RECORD(TYPE, PERFORMANCE),         RECORD(TYPE, MARKER),
    RECORD(TYPE, PUSH_GROUP),  RECORD(TYPE, POP_GROUP),           RECORD(TYPE, OTHER)};

static std::unordered_map<GLenum, std::string> debugSeverityMapping = {
    RECORD3(SEVERITY, HIGH, "high severity"), RECORD3(SEVERITY, MEDIUM, "medium severity"),
    RECORD3(SEVERITY, LOW, "low severity"), RECORD(SEVERITY, NOTIFICATION)};

#undef RECORD
#undef RECORD

auto GetValueSafe(const std::unordered_map<GLenum, std::string>& map, GLenum key) {
  auto iter = map.find(key);
  if (iter == map.cend()) {
    return std::to_string(key);
  }
  return iter->second;
}

TDebugOutput::TDebugOutput()
    : Filter_([](GLenum source, GLenum type, GLuint id, GLenum severity) {
      return severity != GL_DEBUG_SEVERITY_NOTIFICATION;
    }) {}

void TDebugOutput::Attach() {
  std::cout << "OpenGL debug output attached" << std::endl;
  glEnable(GL_DEBUG_OUTPUT);
  glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
  glDebugMessageCallback(TDebugOutput::DebugCallback, this);
}

void GLAPIENTRY TDebugOutput::DebugCallback(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length,
                                            const GLchar* message, const void* userParam) {
  std::string sourceStr = GetValueSafe(debugSourceMapping, source);
  std::string typeStr = GetValueSafe(debugTypeMapping, type);
  std::string severityStr = GetValueSafe(debugSeverityMapping, severity);
  auto debugOutput = static_cast<const TDebugOutput*>(userParam);

  if (debugOutput && !debugOutput->Filter_(source, type, id, severity)) {
    return;
  }

  std::cout << "[DEBUG] " << sourceStr << " " << typeStr << " " << severityStr << " #" << id << ": " << message
            << std::endl;

  assert(type != GL_DEBUG_TYPE_ERROR);
}