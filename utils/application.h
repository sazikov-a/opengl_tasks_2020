#pragma once

#include <imgui.h>
#include <imgui_impl_glfw_gl3.h>

#include "camera.h"
#include "debug_output.h"

#ifndef GLM_FORCE_RADIANS
#define GLM_FORCE_RADIANS
#endif
#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

class TApplication {
 public:
  TApplication();
  ~TApplication();

  void Start();

  virtual void HandleKey(int key, int scancode, int action, int mods);

  virtual void HandleMouseMove(double xPos, double yPos);

  virtual void HandleScroll(double xOffset, double yOffset);

 private:
  void Run();

 protected:
  virtual void InitContext();

  virtual void InitGL();

  virtual void InitGUI();

  virtual void MakeScene();

  virtual void Update();

  virtual void UpdateGUI();

  virtual void Draw();

  virtual void DrawGUI();

  virtual void OnStop() {}

 protected:
  TDebugOutput DebugOutput_;

  TCameraInfo Camera_{};
  TCameraMoverHolder CameraMover_;

  GLFWwindow* Window_ = nullptr;
  double OldTime_ = 0.0;
};
