#include "camera.h"

#include <imgui.h>

#include <glm/gtx/quaternion.hpp>
#include <glm/gtx/transform.hpp>
#include <iostream>

void TOrbitCameraMover::HandleMouseMove(GLFWwindow* window, double xPos, double yPos) {
  if (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT) == GLFW_PRESS) {
    double dx = xPos - OldXPos_;
    double dy = yPos - OldYPos_;

    Phi_ -= dx * 0.005;
    Theta_ += dy * 0.005;

    Theta_ = glm::clamp(Theta_, -glm::pi<double>() * 0.49, glm::pi<double>() * 0.49);
  }

  OldXPos_ = xPos;
  OldYPos_ = yPos;
}

void TOrbitCameraMover::HandleScroll(GLFWwindow* window, double, double yOffset) { R_ += R_ * yOffset * 0.05; }

void TOrbitCameraMover::Update(GLFWwindow* window, double dt) {
  static constexpr double speed = 1.0;

  if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS) {
    Phi_ -= speed * dt;
  }
  if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS) {
    Phi_ += speed * dt;
  }
  if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS) {
    Theta_ += speed * dt;
  }
  if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS) {
    Theta_ -= speed * dt;
  }
  if (glfwGetKey(window, GLFW_KEY_R) == GLFW_PRESS) {
    R_ += R_ * dt;
  }
  if (glfwGetKey(window, GLFW_KEY_F) == GLFW_PRESS) {
    R_ -= R_ * dt;
  }

  Theta_ = glm::clamp(Theta_, -glm::pi<double>() * 0.49, glm::pi<double>() * 0.49);

  int width, height;
  glfwGetFramebufferSize(window, &width, &height);

  Camera_.ViewMatrix = glm::lookAt(
      glm::vec3(glm::cos(Phi_) * glm::cos(Theta_), glm::sin(Phi_) * glm::cos(Theta_), glm::sin(Theta_) + 0.5f) *
          (float)R_,
      glm::vec3(0.0f, 0.0f, 0.5f), glm::vec3(0.0f, 0.0f, 1.0f));
  Camera_.ProjMatrix = glm::perspective(glm::radians(45.0f), (float)width / (float)height, Near_, Far_);
}

void TOrbitCameraMover::ShowOrientationParametersImGui() {
  ImGui::Text("r = %.2f, phi = %.2f, theta = %2f", R_, Phi_, Theta_);
}

TFreeCameraMover::TFreeCameraMover()
    : Position_(5.0f, 0.0f, 2.5f)
    , Rotation_(glm::toQuat(glm::lookAt(Position_, glm::vec3(0.0f, 0.0f, 0.5f), glm::vec3(0.0f, 0.0f, 1.0f)))) {}

void TFreeCameraMover::HandleMouseMove(GLFWwindow* window, double xPos, double yPos) {
  if (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT) == GLFW_PRESS) {
    double dx = xPos - OldXPos_;
    double dy = yPos - OldYPos_;

    Rotation_ *= glm::angleAxis(static_cast<float>(dy * 0.005), glm::vec3(1.0f, 0.0f, 0.0f) * Rotation_);
    Rotation_ *= glm::angleAxis(static_cast<float>(dx * 0.005), glm::vec3(0.0f, 0.0f, 1.0f));
  }

  OldXPos_ = xPos;
  OldYPos_ = yPos;
}

void TFreeCameraMover::Update(GLFWwindow* window, double dt) {
  static constexpr float speed = 1.0f;

  glm::vec3 forwardDir = glm::vec3(0.0f, 0.0f, -1.0f) * Rotation_;
  glm::vec3 rightDir = glm::vec3(1.0f, 0.0f, 0.0f) * Rotation_;

  if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS) {
    Position_ += forwardDir * speed * static_cast<float>(dt);
  }
  if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS) {
    Position_ -= forwardDir * speed * static_cast<float>(dt);
  }
  if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS) {
    Position_ -= rightDir * speed * static_cast<float>(dt);
  }
  if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS) {
    Position_ += rightDir * speed * static_cast<float>(dt);
  }

  int width, height;
  glfwGetFramebufferSize(window, &width, &height);

  Camera_.ViewMatrix = glm::toMat4(-Rotation_) * glm::translate(-Position_);
  Camera_.ProjMatrix = glm::perspective(glm::radians(45.0f), (float)width / (float)height, Near_, Far_);
}
