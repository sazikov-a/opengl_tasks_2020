#pragma once

#include <GL/glew.h>

#include <functional>

class TDebugOutput {
 public:
  using TFilter = std::function<bool(GLenum, GLenum, GLuint, GLenum)>;

 public:
  TDebugOutput();

  static bool IsSupported() { return GLEW_KHR_debug; }

  void Attach();

 private:
  static void GLAPIENTRY DebugCallback(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length,
                                       const GLchar* message, const void* userParam);

 private:
  TFilter Filter_;
};
