#include "texture.h"

#include <SOIL2.h>

#include <cassert>
#include <iostream>
#include <vector>

static void InvertY(unsigned char* image, int width, int height, int channels) {
  for (int j = 0; j * 2 < height; ++j) {
    unsigned int index1 = j * width * channels;
    unsigned int index2 = (height - 1 - j) * width * channels;
    for (int i = 0; i < width * channels; i++) {
      unsigned char temp = image[index1];
      image[index1] = image[index2];
      image[index2] = temp;
      ++index1;
      ++index2;
    }
  }
}

void TTexture::SetTextureImage2D(GLenum target, GLint level, GLint internalFormat, GLsizei width, GLsizei height,
                                 GLenum format, GLenum type, const GLvoid* data) const noexcept {
  glBindTexture(Target_, Texture_);
  glTexImage2D(target, level, internalFormat, width, height, 0, format, type, data);
  glBindTexture(Target_, 0);
}

void TTexture::SetTextureImage1D(GLenum target, GLint level, GLint internalFormat, GLsizei width, GLenum format,
                                 GLenum type, const GLvoid* data) const noexcept {
  glBindTexture(Target_, Texture_);
  glTexImage1D(target, level, internalFormat, width, 0, format, type, data);
  glBindTexture(Target_, 0);
}

void TTexture::InitStorage2D(GLsizei mipmaps, GLint internalFormat, GLsizei width, GLsizei height) const noexcept {
  assert(GLEW_ARB_texture_storage);
  glTextureStorage2D(Texture_, mipmaps, internalFormat, width, height);
}

void TTexture::GenerateMipMaps() const noexcept {
  if (USE_DSA) {
    glGenerateTextureMipmap(Texture_);
  } else {
    glBindTexture(Target_, Texture_);
    glGenerateMipmap(Target_);
    glBindTexture(Target_, 0);
  }
}

void TTexture::AttachToFrameBuffer(GLenum attachment) const noexcept {
  glBindTexture(Target_, Texture_);
  glFramebufferTexture(GL_FRAMEBUFFER, attachment, Texture_, 0);
  glBindTexture(Target_, 0);
}

void TTexture::AttachToFrameBuffer(GLuint fbo, GLenum attachment) const noexcept {
  if (USE_DSA) {
    glNamedFramebufferTexture(fbo, attachment, Texture_, 0);
  } else {
    glBindFramebuffer(GL_FRAMEBUFFER, fbo);
    AttachToFrameBuffer(attachment);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
  }
}

GLenum TTexture::GetInternalFormat() const noexcept {
  GLint internalFormat;
  if (USE_DSA) {
    glGetTextureLevelParameteriv(Texture_, 0, GL_TEXTURE_INTERNAL_FORMAT, &internalFormat);
  } else {
    Bind();
    glGetTexLevelParameteriv(Target_, 0, GL_TEXTURE_INTERNAL_FORMAT, &internalFormat);
    Unbind();
  }
  return static_cast<GLenum>(internalFormat);
}

std::pair<GLsizei, GLsizei> TTexture::GetSize() const noexcept {
  GLsizei width = 0, height = 0;
  if (USE_DSA) {
    glGetTextureLevelParameteriv(Texture_, 0, GL_TEXTURE_WIDTH, &width);
    glGetTextureLevelParameteriv(Texture_, 0, GL_TEXTURE_HEIGHT, &height);
  } else {
    Bind();
    glGetTexLevelParameteriv(Target_, 0, GL_TEXTURE_WIDTH, &width);
    glGetTexLevelParameteriv(Target_, 0, GL_TEXTURE_HEIGHT, &height);
    Unbind();
  }
  return {width, height};
}

void TTexture::SaveAsPng(std::string_view fileName) const {
  int packAlignment;
  glGetIntegerv(GL_PACK_ALIGNMENT, &packAlignment);

  auto [width, height] = GetSize();
  std::vector<unsigned char> pixels(4 * size_t(width) * size_t(height));

  glPixelStorei(GL_PACK_ALIGNMENT, 1);
  Bind();
  glGetTexImage(GL_TEXTURE_2D, 0, GL_RGBA, GL_UNSIGNED_BYTE, pixels.data());

  glPixelStorei(GL_PACK_ALIGNMENT, packAlignment);

  InvertY(pixels.data(), width, height, 4);

  SOIL_save_image(fileName.data(), SOIL_SAVE_TYPE_PNG, width, height, 4, pixels.data());
  std::cout << "save image to " << fileName << ": " << SOIL_last_result() << std::endl;
}

TTexturePtr LoadTextureGL(std::string_view fileName) {
  GLuint texId = SOIL_load_OGL_texture(fileName.data(), 0, 0, SOIL_FLAG_INVERT_Y);
  std::cout << SOIL_last_result() << std::endl;
  TTexturePtr texture = std::make_unique<TTexture>(texId, GL_TEXTURE_2D);
  texture->GenerateMipMaps();
  return texture;
}

TTexturePtr LoadTexture(std::string_view fileName, bool withSRGB, bool prefer1D) {
  int width, height, channels;
  unsigned char* image = SOIL_load_image(fileName.data(), &width, &height, &channels, SOIL_LOAD_AUTO);
  if (!image) {
    throw std::runtime_error(std::string("SOIL loading error: ") + SOIL_last_result());
  }

  InvertY(image, width, height, channels);

  GLint internalFormat;
  if (withSRGB) {
    internalFormat = (channels == 4) ? GL_SRGB8 : GL_SRGB8_ALPHA8;
  } else {
    internalFormat = (channels == 4) ? GL_RGBA8 : GL_RGB8;
  }

  GLint format = (channels == 4) ? GL_RGBA : GL_RGB;

  bool is1DTexture = (height == 1);
  GLenum target = (prefer1D && is1DTexture) ? GL_TEXTURE_1D : GL_TEXTURE_2D;

  TTexturePtr texture = std::make_unique<TTexture>(target);
  if (is1DTexture) {
    texture->SetTextureImage1D(target, 0, internalFormat, width, format, GL_UNSIGNED_BYTE, image);
  } else {
    texture->SetTextureImage2D(target, 0, internalFormat, width, height, format, GL_UNSIGNED_BYTE, image);
  }
  texture->GenerateMipMaps();
  SOIL_free_image_data(image);
  return texture;
}

TTexturePtr LoadTextureDDS(std::string_view fileName) {
  GLuint tex = SOIL_load_OGL_texture(fileName.data(), SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_DDS_LOAD_DIRECT);
  if (tex == 0) {
    throw std::runtime_error(std::string("SOIL loading error: ") + SOIL_last_result());
  }

  return std::make_unique<TTexture>(tex, GL_TEXTURE_2D);
}
