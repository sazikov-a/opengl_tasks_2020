#pragma once

struct TNonCopyable {
  TNonCopyable(const TNonCopyable&) = delete;
  TNonCopyable& operator=(const TNonCopyable&) = delete;

  TNonCopyable() = default;
  ~TNonCopyable() = default;
};

struct TMoveOnly {
  TMoveOnly(TMoveOnly&&) noexcept = default;
  TMoveOnly& operator=(TMoveOnly&&) noexcept = default;

  TMoveOnly(const TMoveOnly&) = delete;
  TMoveOnly& operator=(const TMoveOnly&) = delete;

  TMoveOnly() = default;
  ~TMoveOnly() = default;
};