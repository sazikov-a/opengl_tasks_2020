#pragma once

#include <type_traits>

#include "shader.h"

class TShaderProgram final : public TNonCopyable {
 public:
  TShaderProgram()
      : ProgramId_(glCreateProgram()) {}

  TShaderProgram(std::string_view vertShader, std::string_view fragShader)
      : TShaderProgram() {
    CreateProgram(vertShader, fragShader);
  }

  TShaderProgram(std::string_view vertShader, std::string_view geomShader, std::string_view fragShader)
      : TShaderProgram() {
    CreateProgram(vertShader, geomShader, fragShader);
  }

  ~TShaderProgram() { glDeleteProgram(ProgramId_); }

  void CreateProgram(std::string_view vertShader, std::string_view fragShader);

  void CreateProgram(std::string_view vertShader, std::string_view geomShader, std::string_view fragShader);

  void AttachShader(TShaderHolder shader);

  void LinkProgram();

  [[nodiscard]] auto Id() const noexcept { return ProgramId_; }

  void Use() const noexcept { glUseProgram(ProgramId_); }

  void AssertActive() const {
#ifndef NDEBUG
    GLint currentProgram;
    glGetIntegerv(GL_CURRENT_PROGRAM, &currentProgram);
    assert(currentProgram == ProgramId_);
#endif
  }

  template <class T>
  void SetUniform(std::string_view name, const T& value) const {
    if constexpr (std::is_same_v<T, int>) {
      SetIntUniform(name, value);
    } else if constexpr (std::is_same_v<T, float>) {
      SetFloatUniform(name, value);
    } else if constexpr (std::is_same_v<T, glm::vec2>) {
      SetVec2Uniform(name, value);
    } else if constexpr (std::is_same_v<T, glm::vec3>) {
      SetVec3Uniform(name, value);
    } else if constexpr (std::is_same_v<T, glm::vec4>) {
      SetVec4Uniform(name, value);
    } else if constexpr (std::is_same_v<T, glm::mat3>) {
      SetMat3Uniform(name, value);
    } else if constexpr (std::is_same_v<T, glm::mat4>) {
      SetMat4Uniform(name, value);
    } else {
      assert(false);
    }
  }

 private:
  void SetIntUniform(std::string_view name, int value) const;
  void SetFloatUniform(std::string_view name, float value) const;
  void SetVec2Uniform(std::string_view name, const glm::vec2& vec) const;
  void SetVec3Uniform(std::string_view name, const glm::vec3& vec) const;
  void SetVec4Uniform(std::string_view name, const glm::vec4& vec) const;
  void SetMat3Uniform(std::string_view name, const glm::mat3& mat) const;
  void SetMat4Uniform(std::string_view name, const glm::mat4& mat) const;

 private:
  GLuint ProgramId_;
  std::vector<TShaderHolder> Shaders_;
};

using TShaderProgramHolder = std::unique_ptr<TShaderProgram>;