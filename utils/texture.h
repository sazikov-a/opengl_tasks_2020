#pragma once

#include "common.h"
#include "noncopyable.h"

#ifndef GLM_FORCE_RADIANS
#define GLM_FORCE_RADIANS
#endif
#include <GL/glew.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <memory>
#include <string>
#include <vector>

class TTexture final : public TNonCopyable {
 public:
  explicit TTexture(GLenum target = GL_TEXTURE_2D) noexcept
      : Texture_(0)
      , Target_(target) {
    if (USE_DSA) {
      glCreateTextures(Target_, 1, &Texture_);
    } else {
      glGenTextures(1, &Texture_);
    }
  }

  TTexture(GLuint texture, GLenum target) noexcept
      : Texture_(texture)
      , Target_(target) {}

  ~TTexture() noexcept { glDeleteTextures(1, &Texture_); }

  void SetTextureImage2D(GLenum target, GLint level, GLint internalFormat, GLsizei width, GLsizei height, GLenum format,
                         GLenum type, const GLvoid* data) const noexcept;

  void SetTextureImage1D(GLenum target, GLint level, GLint internalFormat, GLsizei width, GLenum format, GLenum type,
                         const GLvoid* data) const noexcept;

  void InitStorage2D(GLsizei mipmaps, GLint internalFormat, GLsizei width, GLsizei height) const noexcept;

  void GenerateMipMaps() const noexcept;

  void AttachToFrameBuffer(GLenum attachment) const noexcept;

  void AttachToFrameBuffer(GLuint fbo, GLenum attachment) const noexcept;

  void Bind() const noexcept { glBindTexture(Target_, Texture_); }

  void Unbind() const noexcept { glBindTexture(Target_, 0); }

  [[nodiscard]] GLuint GetTexture() const noexcept { return Texture_; }

  [[nodiscard]] GLenum GetInternalFormat() const noexcept;

  [[nodiscard]] std::pair<GLsizei, GLsizei> GetSize() const noexcept;

  void SaveAsPng(std::string_view fileName) const;

 private:
  GLuint Texture_;
  GLenum Target_;
};

using TTexturePtr = std::unique_ptr<TTexture>;

TTexturePtr LoadTextureGL(std::string_view fileName);
TTexturePtr LoadTexture(std::string_view fileName, bool withSRGB = false, bool prefer1D = false);
TTexturePtr LoadTextureDDS(std::string_view fileName);