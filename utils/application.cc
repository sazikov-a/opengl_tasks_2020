#include "application.h"

#include <cstdlib>
#include <iostream>
#include <vector>

#include "common.h"

TApplication::TApplication()
    : CameraMover_(std::make_unique<TOrbitCameraMover>()) {}

TApplication::~TApplication() {
  ImGui_ImplGlfwGL3_Shutdown();
  glfwTerminate();
}

void TApplication::Start() {
  InitContext();
  InitGL();
  InitGUI();
  MakeScene();
  Run();
}

void TApplication::HandleKey(int key, int scancode, int action, int mods) {
  if (action == GLFW_PRESS && key == GLFW_KEY_ESCAPE) {
    glfwSetWindowShouldClose(Window_, GL_TRUE);
  }
  CameraMover_->HandleKey(Window_, key, scancode, action, mods);
}

void TApplication::HandleMouseMove(double xPos, double yPos) {
  if (ImGui::IsMouseHoveringAnyWindow()) {
    return;
  }
  CameraMover_->HandleMouseMove(Window_, xPos, yPos);
}

void TApplication::HandleScroll(double xOffset, double yOffset) {
  CameraMover_->HandleScroll(Window_, xOffset, yOffset);
}

void TApplication::InitContext() {
  if (!glfwInit()) {
    throw std::runtime_error("could not start GLFW3");
  }

  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
  glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
  glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GL_TRUE);

  int monitorsCount;
  GLFWmonitor** monitors = glfwGetMonitors(&monitorsCount);

  GLFWmonitor* demoMonitor = monitors[monitorsCount - 1];
  const GLFWvidmode* videoMode = glfwGetVideoMode(demoMonitor);
  Window_ = glfwCreateWindow(1680, 1050, "MIPT OpenGL", nullptr, nullptr);

  if (!Window_) {
    glfwTerminate();
    throw std::runtime_error("could not start GLFW3");
  }

  glfwMakeContextCurrent(Window_);

  glfwSwapInterval(0);

  glfwSetWindowUserPointer(Window_, this);

  glfwSetKeyCallback(Window_, [](auto window, int key, int scancode, int action, int mods) {
    static_cast<TApplication*>(glfwGetWindowUserPointer(window))->HandleKey(key, scancode, action, mods);
  });
  glfwSetWindowSizeCallback(Window_, [](auto, auto, auto) {});
  glfwSetMouseButtonCallback(Window_, [](auto, auto, auto, auto) {});
  glfwSetCursorPosCallback(Window_, [](auto window, auto xPos, auto yPos) {
    static_cast<TApplication*>(glfwGetWindowUserPointer(window))->HandleMouseMove(xPos, yPos);
  });
  glfwSetScrollCallback(Window_, [](auto window, auto xOffset, auto yOffset) {
    static_cast<TApplication*>(glfwGetWindowUserPointer(window))->HandleScroll(xOffset, yOffset);
  });
}

void TApplication::InitGL() {
  glewExperimental = GL_TRUE;
  glewInit();

  const GLubyte* renderer = glGetString(GL_RENDERER);  //Получаем имя рендерера
  const GLubyte* version = glGetString(GL_VERSION);    //Получаем номер версии
  const GLubyte* glslVersion = glGetString(GL_SHADING_LANGUAGE_VERSION);
  std::cout << "Renderer: " << renderer << std::endl;
  std::cout << "OpenGL context version: " << version << std::endl;
  std::cout << "GLSL version: " << glslVersion << std::endl;

  // Сбросим флаг ошибки.
  glGetError();

  if (GLEW_VERSION_4_5) {
    std::cout << "OpenGL 4.5 supported." << (USE_DSA ? " DSA explicitly enabled" : " DSA explicitly disabled")
              << (USE_INTERFACE_QUERY ? " Interface query explicitly enabled" : " Interface query explicitly disabled")
              << std::endl;
  }

  if (TDebugOutput::IsSupported()) {
    DebugOutput_.Attach();
  }

  glEnable(GL_DEPTH_TEST);
  glDepthFunc(GL_LESS);
}

void TApplication::InitGUI() { ImGui_ImplGlfwGL3_Init(Window_, false); }

void TApplication::MakeScene() {
  Camera_.ViewMatrix = glm::lookAt(glm::vec3(0.0f, -5.0f, 0.0f), glm::vec3(0.0f), glm::vec3(0.0f, 0.0f, 1.0f));
  Camera_.ProjMatrix = glm::perspective(glm::radians(45.0f), 4.0f / 3.0f, 0.1f, 100.f);
}

void TApplication::Run() {
  while (!glfwWindowShouldClose(Window_)) {
    glfwPollEvents();
    Update();
    UpdateGUI();
    Draw();
    DrawGUI();
    glfwSwapBuffers(Window_);
  }
  OnStop();
}

void TApplication::Update() {
  double dt = glfwGetTime() - OldTime_;
  OldTime_ = glfwGetTime();
  CameraMover_->Update(Window_, dt);
  Camera_ = CameraMover_->CameraInfo();
}

void TApplication::UpdateGUI() { ImGui_ImplGlfwGL3_NewFrame(); }

void TApplication::Draw() {}

void TApplication::DrawGUI() { ImGui::Render(); }
