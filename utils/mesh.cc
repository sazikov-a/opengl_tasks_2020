#include "mesh.h"

void TMesh::SetAttribute(GLuint index, GLint size, GLenum type, GLboolean normalized, GLsizei stride, GLuint64 offset,
                         TDataBuffer* buffer) {
  Buffers_[index].reset(buffer);
  glBindVertexArray(Vao_);
  buffer->Bind();
  glEnableVertexAttribArray(index);
  glVertexAttribPointer(index, size, type, normalized, stride, reinterpret_cast<void*>(offset));
  buffer->Unbind();
  glBindVertexArray(0);
}

void TMesh::SetAttributeI(GLuint index, GLint size, GLenum type, GLsizei stride, GLuint64 offset, TDataBuffer* buffer) {
  Buffers_[index].reset(buffer);
  glBindVertexArray(Vao_);
  buffer->Bind();
  glEnableVertexAttribArray(index);
  glVertexAttribIPointer(index, size, type, stride, reinterpret_cast<void*>(offset));
  buffer->Unbind();
  glBindVertexArray(0);
}

void TMesh::SetIndexes(GLuint indexesCount, TDataBufferHolder indexBuffer) {
  HasIndexes_ = true;
  IndexesCount_ = indexesCount;
  IndexBuffer_ = std::move(indexBuffer);
  glBindVertexArray(Vao_);
  IndexBuffer_->Bind();
  glBindVertexArray(0);
}

void TMesh::MultiDraw(GLsizei drawCount) const {
  glBindVertexArray(Vao_);
  if (HasIndexes_) {
    std::vector<GLsizei> counts(drawCount, IndexesCount_);
    std::vector<const GLvoid*> offsets(drawCount, 0);
    glMultiDrawElements(PrimitiveType_, counts.data(), GL_UNSIGNED_INT, offsets.data(), drawCount);
  } else {
    std::vector<GLint> offsets(drawCount, 0);
    std::vector<GLsizei> counts(drawCount, VertexCount_);
    glMultiDrawArrays(PrimitiveType_, offsets.data(), counts.data(), drawCount);
  }
}

void TMesh::DrawInstanced(unsigned int instanceCount) const {
  glBindVertexArray(Vao_);
  if (HasIndexes_) {
    glDrawElementsInstanced(PrimitiveType_, IndexesCount_, GL_UNSIGNED_INT, nullptr, instanceCount);
  } else {
    glDrawArraysInstanced(PrimitiveType_, 0, VertexCount_, instanceCount);
  }
}