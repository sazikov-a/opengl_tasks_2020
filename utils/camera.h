#pragma once

#ifndef GLM_FORCE_RADIANS
#define GLM_FORCE_RADIANS
#endif
#define GLM_ENABLE_EXPERIMENTAL
#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <memory>

struct TCameraInfo {
  glm::mat4 ViewMatrix;
  glm::mat4 ProjMatrix;
};

class TCameraMover {
 public:
  [[nodiscard]] auto CameraInfo() const noexcept { return Camera_; }

  virtual void HandleKey(GLFWwindow* window, int key, int scancode, int action, int mods) = 0;
  virtual void HandleMouseMove(GLFWwindow* window, double xPos, double yPos) = 0;
  virtual void HandleScroll(GLFWwindow* window, double xOffset, double yOffset) = 0;

  virtual void Update(GLFWwindow* window, double dt) = 0;

  virtual void SetNearFarPlanes(float near, float far) {
    Near_ = near;
    Far_ = far;
  }

  virtual void ShowOrientationParametersImGui() {}

 protected:
  TCameraInfo Camera_;
  float Near_ = 0.1f;
  float Far_ = 100.0f;
};

using TCameraMoverHolder = std::unique_ptr<TCameraMover>;

class TOrbitCameraMover final : public TCameraMover {
 public:
  void HandleKey(GLFWwindow*, int, int, int, int) override {}

  void HandleMouseMove(GLFWwindow* window, double xPos, double yPos) override;

  void HandleScroll(GLFWwindow* window, double xOffset, double yOffset) override;

  void Update(GLFWwindow* window, double dt) override;

  void ShowOrientationParametersImGui() override;

  void SetOrientationParameters(double r, double phi, double theta) {
    R_ = r;
    Phi_ = phi;
    Theta_ = theta;
  }

 private:
  double Phi_ = 0.0;
  double Theta_ = 0.0;
  double R_ = 5.0;
  double OldXPos_ = 0.0;
  double OldYPos_ = 0.0;
};

class TFreeCameraMover final : public TCameraMover {
 public:
  TFreeCameraMover();

  void HandleKey(GLFWwindow*, int, int, int, int) override {}

  void HandleMouseMove(GLFWwindow* window, double xPos, double yPos) override;

  void HandleScroll(GLFWwindow*, double, double) override {}

  void Update(GLFWwindow* window, double dt) override;

 private:
  glm::vec3 Position_;
  glm::quat Rotation_;

  double OldXPos_ = 0.0;
  double OldYPos_ = 0.0;
};