#include "shader.h"

#include <fstream>
#include <iostream>
#include <stdexcept>
#include <string>

void TShader::CreateFromFile(std::string_view file) {
  std::ifstream shaderStream(file.data());
  std::string shaderFileContent((std::istreambuf_iterator<char>(shaderStream)), (std::istreambuf_iterator<char>()));
  shaderStream.close();
  CreateFromString(file, shaderFileContent);
}

void TShader::CreateFromString(std::string_view name, std::string_view text) {
  const char* shaderText = text.data();

  glShaderSource(ShaderId_, 1, &shaderText, nullptr);
  glCompileShader(ShaderId_);

  int status = -1;
  glGetShaderiv(ShaderId_, GL_COMPILE_STATUS, &status);

  if (status != GL_TRUE) {
    GLint errorLength;
    glGetShaderiv(ShaderId_, GL_INFO_LOG_LENGTH, &errorLength);

    std::string errorMessage(errorLength, 0);
    glGetShaderInfoLog(ShaderId_, errorLength, nullptr, errorMessage.data());

    throw std::runtime_error("Failed to compile the shader `" + std::string(name) + "`: " + errorMessage);
  }
}

