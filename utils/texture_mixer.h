#pragma once

#include <array>
#include <memory>

#include "noncopyable.h"
#include "shader_program.h"
#include "texture.h"

template <size_t N>
class TTextureMixer final : public TNonCopyable {
 public:
  explicit TTextureMixer(std::array<std::string_view, N> textureFiles) {
    for (size_t i = 0; i < N; ++i) {
      Textures_[i] = LoadTexture(textureFiles[i]);
      glGenSamplers(1, &TextureSamplers_[i]);
      glSamplerParameteri(TextureSamplers_[i], GL_TEXTURE_MAG_FILTER, GL_LINEAR);
      glSamplerParameteri(TextureSamplers_[i], GL_TEXTURE_MIN_FILTER, GL_LINEAR);
      glSamplerParameteri(TextureSamplers_[i], GL_TEXTURE_WRAP_S, GL_REPEAT);
      glSamplerParameteri(TextureSamplers_[i], GL_TEXTURE_WRAP_T, GL_REPEAT);
    }
  }

  void Draw(TShaderProgram* shaderProgram, const std::array<std::string_view, N>& textureNames) {
    for (size_t i = 0; i < N; ++i) {
      auto textureDiffusionUnit = static_cast<GLuint>(i);
      glBindTextureUnit(textureDiffusionUnit, Textures_[i]->GetTexture());
      glBindSampler(textureDiffusionUnit, TextureSamplers_[i]);
      shaderProgram->SetUniform(textureNames[i], (int)i);
    }
  }

 private:
  std::array<GLuint, N> TextureSamplers_;
  std::array<TTexturePtr, N> Textures_;
};

template <size_t N>
using TTextureMixerPtr = std::unique_ptr<TTextureMixer<N>>;

template <size_t N>
TTextureMixerPtr<N> CreateTextureMixer(std::array<std::string_view, N> textureFiles) {
  return std::make_unique<TTextureMixer<N>>(std::move(textureFiles));
}