#pragma once

#include "common.h"
#include "noncopyable.h"

#ifndef GLM_FORCE_RADIANS
#define GLM_FORCE_RADIANS
#endif
#include <GL/glew.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <memory>
#include <string_view>
#include <vector>

class TShader final : public TNonCopyable {
 public:
  explicit TShader(GLenum shaderType)
      : ShaderId_(glCreateShader(shaderType))
      , ShaderType_(shaderType) {}

  ~TShader() { glDeleteShader(ShaderId_); }

  void CreateFromFile(std::string_view file);
  void CreateFromString(std::string_view name, std::string_view text);

  [[nodiscard]] auto Id() const noexcept { return ShaderId_; }

 private:
  GLuint ShaderId_;
  GLenum ShaderType_;
};

using TShaderHolder = std::unique_ptr<TShader>;

