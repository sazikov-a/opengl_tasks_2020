#pragma once

#include "common.h"
#include "noncopyable.h"

#ifndef GLM_FORCE_RADIANS
#define GLM_FORCE_RADIANS
#endif
#include <GL/glew.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <memory>
#include <string>
#include <unordered_map>
#include <vector>

class TDataBuffer final : public TNonCopyable {
 public:
  explicit TDataBuffer(GLenum target = GL_ARRAY_BUFFER)
      : Vbo_(0)
      , Target_(target) {
    if (USE_DSA) {
      glCreateBuffers(1, &Vbo_);
    } else {
      glGenBuffers(1, &Vbo_);
    }
  }

  ~TDataBuffer() { glDeleteBuffers(1, &Vbo_); }

  void SetData(GLsizeiptr size, const GLvoid* data) const {
    Bind();
    glBufferData(Target_, size, data, GL_STATIC_DRAW);
    Unbind();
  }

  void InitStorage(GLsizeiptr size, const GLvoid* data, GLbitfield flags) const {
    assert(USE_DSA);
    glNamedBufferStorage(Vbo_, size, data, flags);
  }

  void Bind() const { glBindBuffer(Target_, Vbo_); }

  void Unbind() const { glBindBuffer(Target_, 0); }

  [[nodiscard]] auto Id() const { return Vbo_; }

 private:
  GLuint Vbo_;
  GLenum Target_;
};

using TDataBufferHolder = std::unique_ptr<TDataBuffer>;

class TMesh final : public TNonCopyable {
 public:
  TMesh()
      : PrimitiveType_(GL_TRIANGLES)
      , VertexCount_(0) {
    glGenVertexArrays(1, &Vao_);
  }

  ~TMesh() { glDeleteVertexArrays(1, &Vao_); }

  void SetAttribute(GLuint index, GLint size, GLenum type, GLboolean normalized, GLsizei stride, GLuint64 offset,
                    TDataBuffer* buffer);

  void SetAttributeI(GLuint index, GLint size, GLenum type, GLsizei stride, GLuint64 offset, TDataBuffer* buffer);

  void SetAttributeDivisor(GLuint index, GLuint divisor) const {
    glBindVertexArray(Vao_);
    glVertexAttribDivisor(index, divisor);
    glBindVertexArray(0);
  }

  void SetPrimitiveType(GLuint type) { PrimitiveType_ = type; }

  void SetVertexCount(size_t count) { VertexCount_ = static_cast<GLuint>(count); }

  void SetIndexes(GLuint indexesCount, TDataBufferHolder indexBuffer);

  [[nodiscard]] auto ModelMatrix() const { return ModelMatrix_; }

  void SetModelMatrix(const glm::mat4& m) { ModelMatrix_ = m; }

  [[nodiscard]] auto TrianglesCount() const {
    if (HasIndexes_)
      return IndexesCount_ / 3;
    else
      return VertexCount_ / 3;
  }

  void Draw() const {
    glBindVertexArray(Vao_);
    if (HasIndexes_) {
      glDrawElements(PrimitiveType_, IndexesCount_, GL_UNSIGNED_INT, nullptr);
    } else {
      glDrawArrays(PrimitiveType_, 0, VertexCount_);
    }
  }

  void DrawArrays(GLint first, GLsizei count) const {
    assert(!HasIndexes_);
    glBindVertexArray(Vao_);
    glDrawArrays(PrimitiveType_, first, count);
  }

  void MultiDraw(GLsizei drawCount) const;

  void MultiDrawArrays(GLsizei drawCount, const std::vector<GLint>& offsets, const std::vector<GLsizei>& counts) const {
    assert(!HasIndexes_);
    glMultiDrawArrays(PrimitiveType_, offsets.data(), counts.data(), drawCount);
  }

  void DrawInstanced(unsigned int instanceCount) const;

  [[nodiscard]] auto VertexCount() const { return VertexCount_; }

  [[nodiscard]] auto Vao() const { return Vao_; }

 private:
  GLuint Vao_{};
  GLuint PrimitiveType_;
  GLuint VertexCount_ = 0;
  GLuint IndexesCount_ = 0;
  bool HasIndexes_ = false;
  glm::mat4 ModelMatrix_{};

  TDataBufferHolder IndexBuffer_;
  std::unordered_map<GLuint, TDataBufferHolder> Buffers_;
};

using TMeshHolder = std::unique_ptr<TMesh>;