#include "shader_program.h"

#include <stdexcept>

void TShaderProgram::CreateProgram(std::string_view vertShader, std::string_view fragShader) {
  auto vs = std::make_unique<TShader>(GL_VERTEX_SHADER);
  auto fs = std::make_unique<TShader>(GL_FRAGMENT_SHADER);

  vs->CreateFromFile(vertShader);
  fs->CreateFromFile(fragShader);

  AttachShader(std::move(vs));
  AttachShader(std::move(fs));

  LinkProgram();
}

void TShaderProgram::CreateProgram(std::string_view vertShader, std::string_view geomShader,
                                   std::string_view fragShader) {
  auto vs = std::make_unique<TShader>(GL_VERTEX_SHADER);
  auto gs = std::make_unique<TShader>(GL_GEOMETRY_SHADER);
  auto fs = std::make_unique<TShader>(GL_FRAGMENT_SHADER);

  vs->CreateFromFile(vertShader);
  vs->CreateFromFile(geomShader);
  vs->CreateFromFile(fragShader);

  AttachShader(std::move(vs));
  AttachShader(std::move(gs));
  AttachShader(std::move(fs));

  LinkProgram();
}

void TShaderProgram::AttachShader(TShaderHolder shader) {
  glAttachShader(ProgramId_, shader->Id());
  Shaders_.push_back(std::move(shader));
}

void TShaderProgram::LinkProgram() {
  glLinkProgram(ProgramId_);

  int status = -1;
  glGetProgramiv(ProgramId_, GL_LINK_STATUS, &status);

  if (status != GL_TRUE) {
    GLint errorLength;
    glGetProgramiv(ProgramId_, GL_INFO_LOG_LENGTH, &errorLength);

    std::string errorMessage(errorLength, 0);
    glGetProgramInfoLog(ProgramId_, errorLength, nullptr, errorMessage.data());

    throw std::runtime_error("Failed to link the program: " + errorMessage);
  }
}

void TShaderProgram::SetIntUniform(std::string_view name, int value) const {
  auto uniformLoc = glGetUniformLocation(ProgramId_, name.data());
  if (USE_DSA)
    glProgramUniform1i(ProgramId_, uniformLoc, value);
  else {
    AssertActive();
    glUniform1i(uniformLoc, value);
  }
}

void TShaderProgram::SetFloatUniform(std::string_view name, float value) const {
  auto uniformLoc = glGetUniformLocation(ProgramId_, name.data());
  if (USE_DSA)
    glProgramUniform1f(ProgramId_, uniformLoc, value);
  else {
    AssertActive();
    glUniform1f(uniformLoc, value);
  }
}

void TShaderProgram::SetVec2Uniform(std::string_view name, const glm::vec2& vec) const {
  auto uniformLoc = glGetUniformLocation(ProgramId_, name.data());
  if (USE_DSA)
    glProgramUniform2fv(ProgramId_, uniformLoc, 1, glm::value_ptr(vec));
  else {
    AssertActive();
    glUniform2fv(uniformLoc, 1, glm::value_ptr(vec));
  }
}

void TShaderProgram::SetVec3Uniform(std::string_view name, const glm::vec3& vec) const {
  auto uniformLoc = glGetUniformLocation(ProgramId_, name.data());
  if (USE_DSA)
    glProgramUniform3fv(ProgramId_, uniformLoc, 1, glm::value_ptr(vec));
  else {
    AssertActive();
    glUniform3fv(uniformLoc, 1, glm::value_ptr(vec));
  }
}

void TShaderProgram::SetVec4Uniform(std::string_view name, const glm::vec4& vec) const {
  auto uniformLoc = glGetUniformLocation(ProgramId_, name.data());
  if (USE_DSA)
    glProgramUniform4fv(ProgramId_, uniformLoc, 1, glm::value_ptr(vec));
  else {
    AssertActive();
    glUniform4fv(uniformLoc, 1, glm::value_ptr(vec));
  }
}

void TShaderProgram::SetMat3Uniform(std::string_view name, const glm::mat3& mat) const {
  auto uniformLoc = glGetUniformLocation(ProgramId_, name.data());
  if (USE_DSA)
    glProgramUniformMatrix3fv(ProgramId_, uniformLoc, 1, GL_FALSE, glm::value_ptr(mat));
  else {
    AssertActive();
    glUniformMatrix3fv(uniformLoc, 1, GL_FALSE, glm::value_ptr(mat));
  }
}

void TShaderProgram::SetMat4Uniform(std::string_view name, const glm::mat4& mat) const {
  auto uniformLoc = glGetUniformLocation(ProgramId_, name.data());
  if (USE_DSA)
    glProgramUniformMatrix4fv(ProgramId_, uniformLoc, 1, GL_FALSE, glm::value_ptr(mat));
  else {
    AssertActive();
    glUniformMatrix4fv(uniformLoc, 1, GL_FALSE, glm::value_ptr(mat));
  }
}
